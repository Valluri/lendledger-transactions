/**
 * Created By :- Akshay Misal
 * Created Date :- 07-08-2018
 * Version :- 1.0.0
 */
var config = require('../../config/config.json');                       // call configration file
var Q = require('q');                                                   // for promise 
var mongo = require('mongoskin');                                       // call mongodb    
var db = mongo.db(config.connectionString, { native_parser: true });    // mongodb connectivity

var StellarSdk = require('stellar-sdk');
var request = require('request');
var stellarNetwork = config.stellarNetwork;
if (stellarNetwork === "test") {
    StellarSdk.Network.useTestNetwork();
} else if (stellarNetwork === "public") {
    StellarSdk.Network.usePublicNetwork();
}
var server = new StellarSdk.Server(config.stellarServer); //stellar server

var service = {};

service.getTxnByAccId = getTxnByAccId;
service.getAllMyTxn = getAllMyTxn;
service.getMyTxnByLimit = getMyTxnByLimit;
service.getMyTxnByOrder = getMyTxnByOrder;
service.getMyTxnByQry = getMyTxnByQry;
service.getTxnDetBetTwoAccounts = getTxnDetBetTwoAccounts;

module.exports = service;

/**
 * @author:Akshay Misal
 * @description:Call the stellar api and get the transaction details by accountId
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getTxnByAccId(req, res) {
    var deferred = Q.defer();

    var accountId = req.query.accountId;

    server.loadAccount(accountId).then(function (account) {
        deferred.resolve(account);
    }).catch(function (err) {
        deferred.reject(err);
    })

    return deferred.promise;
}


/**
 * @author:Akshay Misal
 * @description:Call the stellar api and get the transaction details by accountId
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getAllMyTxn(req, res) {
    var deferred = Q.defer();

    var accountId = req.query.accountId;

    server.transactions()
        .forAccount(accountId)
        .call()
        .then(function (data) {
            deferred.resolve(data)
        })
        .catch(function (err) {
            deferred.reject(err);
        })
    return deferred.promise;
}

/**
 * @author:Akshay Misal
 * @description:Call the stellar api and get the transaction details by accountId.
 * Here the limit is starts with [0] to [n]
 * @param {accountId, limit} req 
 * @param {accountDetails} res 
 */
function getMyTxnByLimit(req, res) {
    var deferred = Q.defer();

    var accountId = req.query.accountId;
    var limit = req.query.limit;
    if (limit > 200) {
        deferred.reject({ error: "Maximun number of records to return is 200." })
    }

    server.transactions()
        .forAccount(accountId)
        .limit(limit)
        .call()
        .then(function (data) {
            deferred.resolve(data)
        })
        .catch(function (err) {
            deferred.reject(err);
        })
    return deferred.promise;
}

/**
 * @author:Akshay Misal
 * @description:Call the stellar api and get the transaction details by accountId with order.
 * @param {accountId, order} req 
 * @param {accountDetails} res 
 */
function getMyTxnByOrder(req, res) {
    var deferred = Q.defer();

    var accountId = req.query.accountId;
    var order = req.query.order;

    if (order != 'asc' || order != 'desc') {
        deferred.reject({ error: "please choose correct order type asc/desc." });
    }

    server.transactions()
        .forAccount(accountId)
        .order(order)
        .call()
        .then(function (data) {
            deferred.resolve(data)
        })
        .catch(function (err) {
            deferred.reject(err);
        })

    return deferred.promise;
}

/**
 * @author:Akshay Misal
 * @description:Call the stellar api and get the transaction details by accountId with order & limit.
 * @param {accountId, order, limit} req 
 * @param {accountDetails} res 
 */
function getMyTxnByQry(req, res) {
    var deferred = Q.defer();

    var accountId = req.query.accountId;
    var order = req.query.order;

    if (order != 'asc' || order != 'desc') {
        deferred.reject({ error: "please choose correct order type asc/desc." });
    }
    var limit = req.query.limit;
    if (limit > 200) {
        deferred.reject({ error: "Maximun number of records to return is 200." })
    }

    server.transactions()
        .forAccount(accountId)
        .limit(limit)
        .order(order)
        .call()
        .then(function (data) {
            deferred.resolve(data)
        })
        .catch(function (err) {
            deferred.reject(err);
        })

    return deferred.promise;
}

/**
 * @author:Akshay Misal
 * @description:Call the stellar api and get the audit details by accountId.
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getTxnDetBetTwoAccounts(req, res) {
    var deferred = Q.defer();

    var accountId = req.query.accountId;
    var destId = req.query.destinationId;
    var order = req.query.order;
    var limit = req.query.limit;

   server.transactions()
        .forAccount(accountId)
        .call()
        .then(function (data) {
            // var txnData = [];
            for (var i = 0; i < data.records.length; i++) {
                var operations = data.records[i]._links.operations.href;
                var txnId = operations.split('/')[4];
                const txnData = getByTxnId(txnId);
                // txnData.push();
                console.log("href => ", txnData);

            }   
            deferred.resolve(txnData)
        })
        .catch(function (err) {
            deferred.reject(err);
        })

    return deferred.promise;
}

async function getByTxnId(txnId) {
    var deferred = Q.defer();

    await server.operations()
    .forTransaction(txnId)
    .call()
    .then(function (operationsResult) {
        console.log(operationsResult.records);
        deferred.resolve(operationsResult.records)
    })
    .catch(function (err) {
        console.log(err)
    })
    
    return deferred.promise;
}