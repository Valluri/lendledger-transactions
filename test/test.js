/** 
 * @author:Akshay Misal
 * @version:0.2
 * @since:03-Aug-2018
*/
var server = require('./../server')
var assert = require('assert');
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();

chai.use(chaiHttp);

var accountId = "GBWX7EGTBZC4I42M6IPA7TY3XRRQQ4OH5I3VUBYK7VQAX7GVGWCMJSMU"
var limit = "Enter_Limit"
var order = "Enter_Order"

//===============svr.transaction.js=============================================================

/**
 * @author:Akshay Misal
 * @link:GET /getTxn
 * @param {accountId} req 
 * @param {JSONObject} res 
 * @description:Get offer by account-id.
 */
describe('Get transaction.', () => {
    it('it should get transaction by accountId.', (done) => {
        chai.request(server)
            .get('/api/getTxn?accountId=' + accountId)
            .end((err, res) => {
                res.body.should.be.a('object'); 
                done();
            });
    });
});

/**
 * @author:Akshay Misal
 * @link:GET /myTxn
 * @param {accountId} req 
 * @param {JSONObject} res 
 * @description:Get offer by account-id.
 */
describe('Get all my transaction.', () => {
    it('it should get my transaction by accountId.', (done) => {
        chai.request(server)
            .get('/api/myTxn?accountId=' + accountId)
            .end((err, res) => {
                res.body.should.be.a('object'); 
                done();
            });
    });
});

/**
 * @author:Akshay Misal
 * @link:GET /api/myTxnByLimit
 * @param {accountId,limit} req 
 * @param {JSONObject} res 
 * @description:Get offer by account-id.
 */
describe('Get transaction by limit.', () => {
    it('it should get transaction by limit.', (done) => {
        chai.request(server)
            .get('/api/myTxnByLimit?accountId=' + accountId+'&limit='+limit)
            .end((err, res) => {
                res.body.should.be.a('object'); 
                done();
            });
    });
});

/**
 * @author:Akshay Misal
 * @link:GET /api/myTxnByOrder
 * @param {accountId,order} req 
 * @param {JSONObject} res 
 * @description:Get offer by account-id.
 */
describe('Get transaction by order.', () => {
    it('it should get transaction by order.', (done) => {
        chai.request(server)
            .get('/api/myTxnByOrder?accountId=' + accountId+'&order='+order)
            .end((err, res) => {
                res.body.should.be.a('object'); 
                done();
            });
    });
});

/**
 * @author:Akshay Misal
 * @link:GET /api/myTxnByQry
 * @param {accountId,limit,order} req 
 * @param {JSONObject} res 
 * @description:Get offer by account-id.
 */
describe('Get transaction by order and limit.', () => {
    it('it should get transaction details by limit and order.', (done) => {
        chai.request(server)
            .get('/api/myTxnByQry?accountId=' + accountId+'&order='+order+'&limit='+limit)
            .end((err, res) => {
                res.body.should.be.a('object'); 
                done();
            });
    });
});