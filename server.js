/**
 * @author: Akshay Misal
 * @since: 08-08-2018
 * Version :- 1.0.0
 */
// call the packages we need
var express = require('express');                       // call express
var app = express();                                    // define our app using express
var bodyParser = require('body-parser');                // configure app to use router()
var router = express.Router();                          // get an instance of the express Router
var txnService = require('./app/api/svr.transaction');          // call genrate keys service 
var config = require('./config/config.json');           // call configration file
var port = process.env.PORT || config.port;             // set our port

// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

router.get('/getTxn', getTxnById);
router.get('/myTxn',getAllMyTxn);
router.get('/myTxnByLimit',getMyTxnByLimit);
router.get('/myTxnByOrder',getMyTxnByOrder);
router.get('/myTxnByQry',getMyTxnByQry);
router.get('/myTxnBetAcc',getTxnBetweenAcc);

module.exports = router;

/**
 * @author:Akshay Misal
 * @description:Get transaction details by accountId
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getTxnById(req, res) {
    txnService.getTxnByAccId(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author:Akshay Misal
 * @description:Get all my transaction details by accountId
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getAllMyTxn(req, res) {
    txnService.getAllMyTxn(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}


/**
 * @author:Akshay Misal
 * @description:Get all my transaction details by accountId with limit
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getMyTxnByLimit(req, res) {
    txnService.getMyTxnByLimit(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author:Akshay Misal
 * @description:Get all my transaction details by accountId with order
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getMyTxnByOrder(req, res) {
    txnService.getMyTxnByOrder(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author:Akshay Misal
 * @description:Get all my transaction details by accountId with query (order&limit)
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getMyTxnByQry(req, res) {
    txnService.getMyTxnByQry(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

/**
 * @author:Akshay Misal
 * @description:Get transaction details between two accounts.
 * @param {accountId} req 
 * @param {accountDetails} res 
 */
function getTxnBetweenAcc(req, res) {
    txnService.getTxnDetBetTwoAccounts(req,res)
    .then(function (data) {
        res.status(200).send(data);
    })
    .catch(function (err) {
        res.status(400).send(err);
    });
}

router.get('/', function (req, res) {                   //for testing the api service (http://localhost:8080/)
    res.json({ message: 'hooray! welcome to our api!' });
});

//---------------------------REGISTER OUR ROUTES ---------------------------
app.use('/api', router);                                // all of our routes will be prefixed with /api

// ================= START THE SERVER=======================================
var server = app.listen(port, function () {
    console.log('Server listening at http://localhost:' + server.address().port);
});

module.exports = server